<div id="global_frontpage_uw_scholar_full">
    <p>The Web Resources site has <a href="https://uwaterloo.ca/web-resources/scholar#documentation">documentation for UWaterloo Scholar</a> users.</p>
</div>
<div id="global_frontpage_uw_scholar_full">
    <h2>Guidelines for use</h2>
    <p>
        Owners of UWaterloo Scholar sites must:
    <ul>
        <li>Only use as a personal website for communicating professional and academic work/achievements with your full name as the site title.</li>
        <li>Only post web content that complies with <a href="https://uwaterloo.ca/web-resources/resources/accessibility">web accessibility at the University of Waterloo</a>.</li>
        <li>Ensure copyright and other legal compliance; ensure consistency with University policy and guidelines; best practice.</li>
        <li>Inform <a href="mailto:rt-ist-wcms@rt.uwaterloo.ca?subject=Delete UWaterloo Scholar web site">WCMS support</a> if you are leaving the University of Waterloo or if the site is no longer required for any reason.</li>
    </ul>
    </p>
</div>
<div id="global_frontpage_uw_scholar_left">
    <h2>Why UWaterloo Scholar?</h2>
    <p>
        <b>Easy to create and maintain websites</b><br />
        It takes seconds to create a website.  Friendly user interface that makes it easy to maintain websites from anywhere through a web browser, without any technical expertise.
    </p>
    <p>
        <b>Open source and strong community</b><br />
        Built on top of Drupal and a full stack of open source tools and technologies.  Used by many academic institutions worldwide.
    </p>
    <p><b>Scalable solution</b><br />
        A multitenant architecture that allows academic institutions to host thousands of websites in a single instance of the application.
    </p>
</div>
<div id="global_frontpage_uw_scholar_right">
    <h2>Benefits of UWaterloo Scholar</h2>
    <p>This new service will allow faculty members and researchers to quickly and easily build personal websites using an interface that is logical and intuitive, and does not require previous programming or coding experience.</p>
    <p>Faculty and Researcher sites deliver a secure, professional environment for the promotion of academic work and achievements, while fostering online collaboration opportunities and providing an enhanced end-user experience for site visitors.</p>
    <p>The easy to use site tools and features allow users to self-manage their content, creating site sections unique to the needs of each user. Whether it’s building a robust Publications library, creating an interactive blog post, or advertising upcoming events or workshops, the Waterloo Faculty and Researcher site template will meet your needs.</p>
    </p>
</div>
