<?php

/**
 * @file
 */
?>
<div id="global_frontpage_uw_scholar"><h1>UWATERLOO STAFF</h1></div>
<div id="global_frontpage_uw_scholar_left">
    <h2>What is UWaterloo Staff?</h2>
    <p>The University of Waterloo is using OpenScholar, an open source software solution built on Drupal, to provide dynamic and customizable personal websites for Waterloo Staff. These staff sites are offered as Software as a Service (SaaS) and will be built using the WatIAM of the staff member.</p>
</div>
<div id="global_frontpage_uw_scholar_right">
    <?php if (user_is_logged_in()): ?>
        <h2>Welcome, <b><?php print variable_get('user_first_name'); ?> <?php print variable_get('user_last_name'); ?></b>.</h2>
        <?php if(variable_get('is_admin') == 'yes'): ?>
            <?php if(variable_get('user_has_site_html') !== ''): ?>
                <p>
                    <?php print variable_get('user_has_site_html'); ?>
                </p>
            <?php endif; ?>
            <?php if(variable_get('user_has_site_html') == '' || (variable_get('user_has_site_html') !== '' && variable_get('os_vsites_per_user') == 10000)): ?>
                <div class="uw_button">
                    <div class="uw_button_inner">
                        <a href="site/register">Create a Site</a>
                    </div>
                </div>
                Please click on button above to create a site on UWaterloo Staff.
            <?php endif; ?>
        <?php endif; ?>
    <?php else: ?>
        <h2>Welcome to <b>UWaterloo Scholar</b>.</h2>
        <div class="uw_button">
            <div class="uw_button_inner">
                <a href="<?php print variable_get('cas_login_url'); ?>">Log in</a>
            </div>
        </div>
        Please log in to create a site on UWaterloo Staff.
        <br />
    <?php endif; ?>
</div>
