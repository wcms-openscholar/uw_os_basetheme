<?php

/**
 * @file
 * Template to display a page for UWaterloo Scholar.
 */
?>

<div id="page_wrap" class="page_wrap">
  <!--REGION TO HOUSE RESPONSIVE MENU. OTHER CONTENT CAN'T BE PLACED HERE-->
  <div class="responive-menu-container clearfix">

    <?php print render($page['responsive_menu']); ?>

    <div class="uw-site--off-canvas">
      <div class="uw-section--inner">

        <input type="checkbox" id="responsive-nav-check" class="responsive-nav-check">

        <div id="block-responsive-menu-combined-responsive-menu-combined" class="responsive-nav-menu">
          <div class="content">
            <div class="tabs">
              <div class="tab tab-2">
                <input type="radio" id="tab-main-menu" checked="checked" name="tab-group-responsive_menu_combined">
                  <label for="tab-main-menu">THIS SITE</label>
                  <div class="content">
                    <?php print $page['responsive_menu_html']; ?>
                  </div>
                </div>

                <div class="tab tab-2">
                  <input type="radio" id="tab-menu-uw-menu-global-header" name="tab-group-responsive_menu_combined">
                  <label for="tab-menu-uw-menu-global-header">uWaterloo</label>
                  <div class="content">
                    <ul class="menu">
                      <li class="first leaf">
                        <a href="https://uwaterloo.ca/admissions" title="">Admissions</a>
                      </li>
                      <li class="leaf">
                        <a href="https://uwaterloo.ca/about" title="">About Waterloo</a>
                      </li>
                      <li class="leaf">
                        <a href="https://uwaterloo.ca/faculties-academics" title="">Faculties &amp; academics</a>
                      </li>
                      <li class="leaf">
                        <a href="https://uwaterloo.ca/offices-services" title="">Offices &amp; services</a>
                      </li>
                      <li class="leaf">
                        <a href="https://uwaterloo.ca/support" title="">Support Waterloo</a>
		      </li>
                      <li class="last">
                        <a href="https://uwaterloo.ca/coronavirus" title="">COVID-19</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!--FLEXIBLE ADMIN HEADER FOR USE BY SELECT GROUPS USING OS-->
    <div class="uw-site--inner">
      <div class="uw-section--inner">
        <div id="skip" class="skip">
          <a href="#main" class="element-invisible element-focusable uw-site--element__invisible  uw-site--element__focusable" accesskey="S"><?php print t('Skip to main'); ?></a>
          <a href="#footer" class="element-invisible element-focusable  uw-site--element__invisible  uw-site--element__focusable"><?php print t('Skip to footer'); ?></a>
        </div>
      </div>

      <div id="header" class="uw-header--global">
        <div class="uw-section--inner">
          <div id="branding_header" data-nav-visible="false">
            <div class="branding-container clearfix">
              <div id="uw-header" class="uw-header">
                <div id="uw-header-content" class="uw-header--content">
                  <div id="uw-header-logo" class="uw-header--logo">
                    <a id="uw-logo" class="uw-logo" href="//uwaterloo.ca/" accesskey="1">University of Waterloo</a>
                  </div>

                  <nav class="nav-university uw-header--nav__university" aria-label="university navigation">
                    <ul class="global-menu">
                      <li><a href="https://uwaterloo.ca/admissions">Admissions</a></li>
                      <li><a href="https://uwaterloo.ca/about">About Waterloo</a></li>
                      <li><a href="https://uwaterloo.ca/faculties-academics">Faculties &amp; academics</a></li>
                      <li><a href="https://uwaterloo.ca/offices-services">Offices &amp; services</a></li>
                      <li><a href="https://campaign.uwaterloo.ca">Support Waterloo</a></li>
                      <li><a href="https://uwaterloo.ca/coronavirus">COVID-19</a></li>
                    </ul>
                  </nav>

                  <div id="uw-header-buttons" class="uw-header--buttons">

                    <div id="uw-header-nav-button" class="uw-header--buttons__nav">
                      <div class="navigation-button">
                        <label for="responsive-nav-check" class="navigation-button__toggle" aria-label="navigation menu" aria-controls="navigation" aria-expanded="false" onclick="" title="Menu">
                          Menu
                        </label>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div id="site-colors" class="uw-site--colors">
        <div class="uw-site--cbar
          <?php
            if(variable_get('display_os_uw_homepage')) {
              print variable_get('uw_theme_setting');
            } else {
              print "default";
            }
          ?>
        ">
          <div class="uw-site--c1 uw-cbar"></div>
          <div class="uw-site--c2 uw-cbar"></div>
          <div class="uw-site--c3 uw-cbar"></div>
          <div class="uw-site--c4 uw-cbar"></div>
        </div>
      </div>

      <div id="page" class="container <?php print $classes; ?>">
        <div id="page-wrapper" >

          <?php print $messages; ?>

          <?php if ($page['header_top'] || $page['header_first'] || $page['header_second'] || $page['header_third'] || $page['header_bottom']): ?>

            <!--header regions beg-->
            <header id="header" class="clearfix" role="banner">
              <div id="header-container" class="<?php print variable_get('uw_theme_setting'); ?>">

                <?php if (variable_get('is_global_frontpage') == "yes"): ?>
                  <?php if (variable_get('uwaterloo_scholar_type') == "Staff"): ?>
                    <?php include 'page-uwaterloo-staff-header.tpl.php'; ?>
                  <?php else: ?>
                    <?php include 'page-uwaterloo-scholar-header.tpl.php'; ?>
                  <?php endif; ?>
                <?php endif; ?>

                <div id="header-panels" class="at-panel gpanel panel-display three-col clearfix">
                  <?php print render($page['header_top']); ?>
                  <?php print render($page['header_second']); ?>
                  <?php print render($page['header_bottom']); ?>
                </div>

              </div>
            </header><!--header regions end-->
          <?php endif; ?>

          <?php if(variable_get('is_vsite_home_page') == 'yes' && isset($page['uw_virtual_page_homepage_banners_html']) && variable_get('display_os_uw_homepage')): ?>

            <div class="homepage-banners-top-wrapper <?php print variable_get('uw_theme_setting'); ?>">
              <?php print $page['uw_virtual_page_homepage_banners_html']; ?>
            </div>

          <?php endif; ?>

          <?php if ($menu_bar = render($page['menu_bar'])): ?>
            <div id="menu-wrapper" class="<?php print variable_get('uw_theme_setting'); ?>">
              <!--main menu region beg-->
              <?php print $menu_bar; ?>
              <!--main menu region end-->

              <?php if ($help = render($page['help'])): ?>
                <!--help region beg-->
                <?php print $help; ?>
                <!--help region end-->
              <?php endif; ?>
            </div>
          <?php endif; ?>

          <?php
            $twitter_sidebar = FALSE;
            $twitter_homepage = FALSE;

            if (isset($page['uw-virtual-site-homepage']['field_uw_twitter_username'])
              && isset($page['uw-virtual-site-homepage']['field_uw_twitter_type'])) {

              foreach ($page['uw-virtual-site-homepage']['field_uw_twitter_type']['#items'] as $types) {

                if ($types['value'] == "Sidebar") {
                  $twitter_sidebar = TRUE;
                }
                if ($types['value'] == "Homepage") {
                  $twitter_homepage = TRUE;
                }
              }
            }
          ?>

          <div id="columns" class="clearfix">
            <div class="hg-container clearfix">
              <div id="content-column" role="main">
                <?php if (variable_get('is_vsite_home_page') == 'yes') { ?>
                  <?php if ($twitter_sidebar && !$twitter_homepage) { ?>
                    <div class="content-inner with-sidebar">
                  <?php } else { ?>
                    <div class="content-inner">
                  <?php } ?>
                <?php } else { ?>
                  <?php if ($twitter_sidebar) { ?>
                    <div class="content-inner with-sidebar">
                  <?php } else { ?>
                    <div class="content-inner">
                  <?php } ?>
                <?php } ?>

                <?php
                  if ($breadcrumb) {
                    print $breadcrumb;
                  }
                ?>

                <!-- Add content for personal site homepage -->
                <?php if(variable_get('is_vsite_home_page') == 'yes'
                  && isset($page['uw-virtual-site-homepage'])
                  && variable_get('display_os_uw_homepage')): ?>

                  <div class="personal_frontpage_uw_scholar">
                    <div class="uw_scholar_row">
                      <div class="personal_fp_info">

                        <?php if(isset($page['uw-virtual-site-homepage']['field_uw_profile_image'])): ?>

                          <?php
                            $src = file_create_url($page['uw-virtual-site-homepage']['field_uw_profile_image'][0]['#item']['uri']);
                            if (isset($page['uw-virtual-site-homepage']['body']['#object']->field_profile_image[LANGUAGE_NONE][0]['alt'])) {
                              $alt = $page['uw-virtual-site-homepage']['body']['#object']->field_profile_image[LANGUAGE_NONE][0]['alt'];
                            }
                            else {
                              $alt = '';
                            }
                          ?>

                          <img src="<?php print $src; ?>" alt="<?php print $alt; ?>" class="image-left personal_fp_img"/>

                        <?php endif; ?>

                        <?php if(isset($page['uw-virtual-site-homepage']['field_uw_site_description'])): ?>
                          <?php print $page['uw-virtual-site-homepage']['field_uw_site_description'][0]['#markup']; ?>
                        <?php endif; ?>

                      </div>
                    </div>
                  </div>

                <?php endif; ?>

                <!--personal site homepage/> -->
                <?php if ($is_front || $use_content_regions): ?>

                  <?php print render($title_prefix);?>
                  <a name="<?php echo $skip_link; ?>"></a>

                  <?php if (!$is_front && $title): ?>

                    <header id="main-content-header">
                      <a name="<?php echo $skip_link; ?>"></a>
                      <h1 id="page-title"<?php print $attributes; ?>>
                        <?php print $title; ?>
                      </h1>
                    </header>
                  <?php endif; ?>

                  <?php print render($title_suffix); ?>

                  <?php if ($page['content_top']
                    || $page['content_first']
                    || $page['content_second']
                    || $page['content_bottom']): ?>

                    <!--front panel regions beg-->
                    <div id="content-panels" class="at-panel gpanel panel-display content clearfix">

                      <?php print render($page['content_top']); ?>
                      <?php print render($page['content_first']); ?>
                      <?php print render($page['content_second']); ?>
                      <?php print render($page['content_bottom']); ?>

                    </div>
                    <!--front panel regions end-->

                  <?php endif; ?>
                <?php endif; ?>

                <?php if (variable_get('is_global_frontpage') == "yes"): ?>

                  <div class="global_frontpage_uw_scholar_full">

                    <p>
                      <a href="<?php print strtolower(variable_get('uwaterloo_scholar_type', 'Scholar')) . '-directory'; ?>"><?php print variable_get('uwaterloo_scholar_type', 'Scholar'); ?> directory</a>
                    </p>

                  </div>

                <?php endif; ?>

                <?php if (!$is_front && !$use_content_regions): ?>

                  <<?php print $tag; ?> id="main-content">
                    <a name="<?php echo $skip_link; ?>"></a>
                    <?php print render($title_prefix); ?>

                    <?php if ($title
                      || $primary_local_tasks
                      || $secondary_local_tasks
                      || $action_links = render($action_links)): ?>

                      <header id="main-content-header">

                        <?php if (!$is_front && $title): ?>

                          <h1 id="page-title"<?php print $attributes; ?>>
                            <?php print $title; ?>
                          </h1>

                        <?php endif; ?>

                        <?php if ($primary_local_tasks || $secondary_local_tasks || $action_links): ?>

                          <div id="tasks">

                            <?php if ($primary_local_tasks): ?>
                              <ul class="tabs primary clearfix"><?php print render($primary_local_tasks); ?></ul>
                            <?php endif; ?>

                            <?php if ($secondary_local_tasks): ?>
                              <ul class="tabs secondary clearfix"><?php print render($secondary_local_tasks); ?></ul>
                            <?php endif; ?>

                            <?php if ($action_links = render($action_links)): ?>
                              <ul class="action-links clearfix"><?php print $action_links; ?></ul>
                            <?php endif; ?>

                          </div>
                        <?php endif; ?>

                      </header>

                    <?php endif; ?>

                    <?php print render($title_suffix); ?>

                    <?php if ($content = render($page['content'])): ?>

                      <div id="content" class="<?php print variable_get('uw_theme_setting'); ?>">
                        <?php print $content; ?>
                      </div>

                    <?php endif; ?>

                  </<?php print $tag; ?>><!--main content ends-->

                <?php endif; ?>

              </div><!-- end of content-inner-->

            </div><!--end of content column-->

            <?php if ($sidebar_first = render($page['sidebar_first'])): ?>

              <!--sidebar first region beg-->
              <?php print $sidebar_first; ?>

              <!--sidebar first region end-->
            <?php endif; ?>

            <?php if ($sidebar_second = render($page['sidebar_second'])): ?>

              <!--sidebar second region beg-->
              <?php if (variable_get('is_vsite_home_page') == 'yes') { ?>

                <?php if($twitter_sidebar && !$twitter_homepage && variable_get('display_os_uw_homepage')) { ?>

                  <?php
                    $search = '</div></div></div></div>';
                    $replace = '<div class="twitter-sidebar-wrapper"><a class="twitter-timeline" href="https://twitter.com/' . $page['uw-virtual-site-homepage']['field_uw_twitter_username']['#items'][0]['value'] . '">A Twitter List by ' . $page['uw-virtual-site-homepage']['field_uw_twitter_username']['#items'][0]['value'] . '</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></div>';
                    $sidebar_second = str_replace($search, $replace . $search, $sidebar_second);
                  ?>

                <?php } ?>

              <?php } else { ?>

                <?php if ($twitter_sidebar && variable_get('display_os_uw_homepage')) { ?>
                  <?php
                    $search = '</a></div></div></div></div>';
                    $replace = '</a><div class="twitter-sidebar-wrapper"><a class="twitter-timeline" href="https://twitter.com/' . $page['uw-virtual-site-homepage']['field_uw_twitter_username']['#items'][0]['value'] . '">A Twitter List by ' . $page['uw-virtual-site-homepage']['field_uw_twitter_username']['#items'][0]['value'] . '</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></div></div></div></div></div>';
                    $sidebar_second = str_replace($search, $replace, $sidebar_second);
                  ?>

                <?php } ?>

              <?php } ?>

              <?php print $sidebar_second ?>

              <!--sidebar second region end-->
            <?php else: ?>

              <?php if (variable_get('is_vsite_home_page') == 'yes') { ?>

                <?php if ($twitter_sidebar
                  && !$twitter_homepage
                  && variable_get('display_os_uw_homepage')) { ?>

                  <div class="region region-sidebar-second sidebar sidebar-twitter">
                    <div class="region-inner clearfix">
                      <div class="twitter-sidebar-wrapper">

                        <a class="twitter-timeline" href="https://twitter.com/<?php print $page['uw-virtual-site-homepage']['field_uw_twitter_username']['#items'][0]['value']; ?>">A Twitter List by <?php print $page['uw-virtual-site-homepage']['field_uw_twitter_username']['#items'][0]['value']; ?></a>
                        <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

                      </div>
                    </div>
                  </div>

                <?php } ?>

              <?php } else { ?>

                <?php if ($twitter_sidebar && variable_get('display_os_uw_homepage')) { ?>

                  <div class="region region-sidebar-second sidebar sidebar-twitter">
                    <div class="region-inner clearfix">
                      <div class="twitter-sidebar-wrapper">

                        <a class="twitter-timeline" href="https://twitter.com/<?php print $page['uw-virtual-site-homepage']['field_uw_twitter_username']['#items'][0]['value']; ?>">A Twitter List by <?php print $page['uw-virtual-site-homepage']['field_uw_twitter_username']['#items'][0]['value']; ?></a>
                        <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

                      </div>
                    </div>
                  </div>

                <?php } ?>

              <?php } ?>

            <?php endif; ?>

              <?php if(variable_get('is_vsite_home_page') == 'yes'
                && $twitter_homepage
                && variable_get('display_os_uw_homepage')) { ?>

                <div class="twitter-homepage-wrapper">
                  <a class="twitter-timeline" href="https://twitter.com/<?php print $page['uw-virtual-site-homepage']['field_uw_twitter_username']['#items'][0]['value']; ?>">A Twitter List by <?php print $page['uw-virtual-site-homepage']['field_uw_twitter_username']['#items'][0]['value']; ?></a>
                  <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                </div>

              <?php } ?>

              </div><!-- end of hg-container-->

              <?php if (variable_get('is_global_frontpage') == "yes"): ?>

                <?php if (variable_get('uwaterloo_scholar_type') == "Staff"): ?>
                  <?php include 'page-uwaterloo-staff-text.tpl.php'; ?>
                <?php else: ?>
                  <?php include 'page-uwaterloo-scholar-text.tpl.php'; ?>
                <?php endif; ?>

                <div class="uw_os_data">
                  <?php if (variable_get('uw_os_total_sites') !== ''): ?>
                    <div class="uw_os_total_sites">
                      <h3>Sites</h3>
                      <img src="<?php print drupal_get_path('module', 'uw_cfg_openscholar'); ?>/images/sites.png"/><br />
                      <?php print variable_get('uw_os_total_sites'); ?>
                    </div>
                  <?php endif; ?>

                  <?php if (variable_get('uw_os_total_users') !== ''): ?>
                    <div class="uw_os_total_users">
                      <h3>Users</h3>
                      <img src="<?php print drupal_get_path('module', 'uw_cfg_openscholar'); ?>/images/users.png"/><br />
                      <?php print variable_get('uw_os_total_users'); ?>
                    </div>
                  <?php endif; ?>

                  <?php if (variable_get('uw_os_total_pages') !== ''): ?>
                    <div class="uw_os_total_pages">
                      <h3>Pages</h3>
                      <img src="<?php print drupal_get_path('module', 'uw_cfg_openscholar'); ?>/images/pages.png"/><br />
                      <?php print variable_get('uw_os_total_pages'); ?>
                    </div>
                  <?php endif; ?>

                  <?php if (variable_get('uw_os_total_publications') !== ''): ?>
                    <div class="uw_os_total_posts">
                      <h3>Publications</h3>
                      <img src="<?php print drupal_get_path('module', 'uw_cfg_openscholar'); ?>/images/posts.png"/><br />
                      <?php print variable_get('uw_os_total_publications'); ?>
                    </div>
                  <?php endif; ?>

                </div>

              <?php endif; ?>

            </div>
            <!--footer region beg-->

            <footer id="footer" class="footer-wrap clearfix" role="contentinfo">

              <!-- Three column 3x33 Gpanel -->
              <?php if ($page['footer_top']
                || $page['footer_first']
                || $page['footer']
                || $page['footer_third']
                || $page['footer_bottom']): ?>

                <div class="at-panel gpanel panel-display footer clearfix">
                  <?php print render($page['footer_top']); ?>
                  <?php print render($page['footer_first']); ?>
                  <?php print render($page['footer']); ?>
                  <?php print render($page['footer_third']); ?>
                  <?php print render($page['footer_bottom']); ?>
                </div>

                <!--footer region end-->
              <?php endif; ?>

            </footer>

          </div>
        </div><!--page area ends-->

        <?php if (isset($page['uw-virtual-site-homepage']) && variable_get('display_os_uw_homepage')) : ?>
          <div class="uw_scholar_row_wrapper <?php print variable_get('uw_theme_setting'); ?>">
            <div class="uw_scholar_row">
              <div class="personal_fp_info_footer">
                <div class="personal_fp_school">
                  <div class="faculty-logo">

                    <?php if (isset($page['uw-virtual-site-homepage']['field_uw_faculty_list_logo'][0]['#markup'])) { ?>

                      <?php if ($page['uw-virtual-site-homepage']['field_uw_faculty_list_logo'][0]['#markup'] !== "Other") { ?>

												<?php
													$url = 'uw-logos-site-footer/' . $page['uw-virtual-site-homepage']['field_uw_faculty_list_logo'][0]['#markup'];
													$site_footer_info = _uw_virtual_site_homepage_get_request($url);

													print '<img src="' . $site_footer_info[0]['site_footer_logo']['src'] . '" alt="' . $site_footer_info[0]['site_footer_logo']['alt'] . '"  />';
												?>

                      <?php } else { ?>

                        <?php if(isset($page['uw-virtual-site-homepage']['field_uw_other_faculty'][0]['#markup'])): ?>

                          <?php print '<p>' . $page['uw-virtual-site-homepage']['field_uw_other_faculty'][0]['#markup'] . '</p>'; ?>

                        <?php endif; ?>

                        <?php if (isset($page['uw-virtual-site-homepage']['field_uw_department']['#items'][0]['value'])): ?>

                          <?php print '<p>' . $page['uw-virtual-site-homepage']['field_uw_department']['#items'][0]['value'] . '</p>'; ?>

                        <?php endif; ?>

                      <?php } ?>
                    <?php } else { ?>

                      <h2>University of Waterloo</h2>
                    <?php } ?>

                  </div>
                </div>

                <?php if (isset($page['uw-virtual-site-homepage']['field_uw_office_hours'])): ?>

                  <?php if (isset($page['uw-virtual-site-homepage']['field_uw_office_location'])
                    || isset($page['uw-virtual-site-homepage']['field_uw_phone_number'][0]['#markup'])
                    || isset($page['uw-virtual-site-homepage']['field_uw_email_homepage'])): ?>

                    <div class="personal_fp_contact">
                      <h2>Contact</h2>
                      <div>
                        <?php if(isset($page['uw-virtual-site-homepage']['field_uw_office_location'])): ?>
                          <span><?php print render($page['uw-virtual-site-homepage']['field_uw_office_location']); ?></span>
                        <?php endif; ?>

                        <?php if(isset($page['uw-virtual-site-homepage']['field_uw_phone_number'][0]['#markup'])
                          && isset($page['uw-virtual-site-homepage']['field_uw_extension'][0]['#markup'])): ?>

                          <span><?php print $page['uw-virtual-site-homepage']['field_uw_phone_number'][0]['#markup'] . ', ext. ' . $page['uw-virtual-site-homepage']['field_uw_extension'][0]['#markup']; ?></span>

                        <?php elseif(isset($page['uw-virtual-site-homepage']['field_uw_phone_number'])): ?>

                          <span><?php print render($page['uw-virtual-site-homepage']['field_uw_phone_number']); ?></span>

                        <?php endif; ?>

                        <?php if(isset($page['uw-virtual-site-homepage']['field_uw_email_homepage'])): ?>
                          <span><a href="mailto:<?php print $page['uw-virtual-site-homepage']['field_uw_email_homepage'][0]['#markup']; ?>"><?php print $page['uw-virtual-site-homepage']['field_uw_email_homepage'][0]['#markup']; ?></a></span>
                        <?php endif; ?>

                      </div>
                    </div>

                    <div class="personal_fp_hours">
                      <h2>Office Hours</h2>
                      <div>
                        <?php print render($page['uw-virtual-site-homepage']['field_uw_office_hours']); ?>
                      </div>

                    </div>

                  <?php else: ?>
                      <div class="personal_fp_contact">
                        <h2>&nbsp;</h2>
                      </div>

                      <div class="personal_fp_hours">
                        <h2>Office Hours</h2>
                        <div>
                          <?php print render($page['uw-virtual-site-homepage']['field_uw_office_hours']); ?>
                        </div>
                      </div>

                  <?php endif; ?>

                <?php else: ?>

                  <?php if (isset($page['uw-virtual-site-homepage']['field_uw_office_location'])
                    || isset($page['uw-virtual-site-homepage']['field_uw_phone_number'][0]['#markup'])
                    || isset($page['uw-virtual-site-homepage']['field_uw_email_homepage'])): ?>

                    <?php if (isset($page['uw-virtual-site-homepage']['field_uw_email_homepage'])): ?>

                      <?php if (isset($page['uw-virtual-site-homepage']['field_uw_office_location'])
                        && isset($page['uw-virtual-site-homepage']['field_uw_phone_number'][0]['#markup'])): ?>

                        <div class="personal_fp_contact">
                          <h2>Contact</h2>
                        <div>

                        <span><?php print render($page['uw-virtual-site-homepage']['field_uw_office_location']); ?></span>

                        <?php if (isset($page['uw-virtual-site-homepage']['field_uw_extension'][0]['#markup'])): ?>

                          <span><?php print $page['uw-virtual-site-homepage']['field_uw_phone_number'][0]['#markup'] . ', ext. ' . $page['uw-virtual-site-homepage']['field_uw_extension'][0]['#markup']; ?></span>

                        <?php else: ?>

                          <span><?php print $page['uw-virtual-site-homepage']['field_uw_phone_number'][0]['#markup']; ?>

                        <?php endif ?>



                        <div class="personal_fp_hours">
                          <h2>&nbsp;</h2>
                          <div>
                            <span><a href="mailto:<?php print $page['uw-virtual-site-homepage']['field_uw_email_homepage'][0]['#markup']; ?>"><?php print $page['uw-virtual-site-homepage']['field_uw_email_homepage'][0]['#markup']; ?></a></span>
                          </div>
                        </div>

                      <?php endif; ?>

                      <?php if (isset($page['uw-virtual-site-homepage']['field_uw_office_location'])
                        && !isset($page['uw-virtual-site-homepage']['field_uw_phone_number'][0]['#markup'])): ?>

                        <div class="personal_fp_contact">
                          <h2>Contact</h2>
                          <div>
                            <span><?php print render($page['uw-virtual-site-homepage']['field_uw_office_location']); ?></span>
                          </div>
                        </div>

                        <div class="personal_fp_hours">
                          <h2>&nbsp;</h2>
                          <div>
                            <span><a href="mailto:<?php print $page['uw-virtual-site-homepage']['field_uw_email_homepage'][0]['#markup']; ?>"><?php print $page['uw-virtual-site-homepage']['field_uw_email_homepage'][0]['#markup']; ?></a></span>
                          </div>
                        </div>

                      <?php endif; ?>

                      <?php if(!isset($page['uw-virtual-site-homepage']['field_uw_office_location'])
                        && isset($page['uw-virtual-site-homepage']['field_uw_phone_number'][0]['#markup'])): ?>

                        <div class="personal_fp_contact">
                          <h2>Contact</h2>
                          <div>

                            <?php if (isset($page['uw-virtual-site-homepage']['field_uw_extension'][0]['#markup'])): ?>
                              <span><?php print $page['uw-virtual-site-homepage']['field_uw_phone_number'][0]['#markup'] . ', ext. ' . $page['uw-virtual-site-homepage']['field_uw_extension'][0]['#markup']; ?></span>

                            <?php else: ?>
                              <span><?php print $page['uw-virtual-site-homepage']['field_uw_phone_number'][0]['#markup']; ?></span>
                            <?php endif ?>

                          </div>
                        </div>

                        <div class="personal_fp_hours">
                          <h2>&nbsp;</h2>
                          <div>
                            <span><a href="mailto:<?php print $page['uw-virtual-site-homepage']['field_uw_email_homepage'][0]['#markup']; ?>"><?php print $page['uw-virtual-site-homepage']['field_uw_email_homepage'][0]['#markup']; ?></a></span>
                          </div>
                        </div>

                      <?php endif; ?>

                      <?php if (!isset($page['uw-virtual-site-homepage']['field_uw_office_location'])
                        && !isset($page['uw-virtual-site-homepage']['field_uw_phone_number'][0]['#markup'])): ?>

                        <div class="personal_fp_contact">
                          <h2>Contact</h2>
                          <div>
                            <span><a href="mailto:<?php print $page['uw-virtual-site-homepage']['field_uw_email_homepage'][0]['#markup']; ?>"><?php print $page['uw-virtual-site-homepage']['field_uw_email_homepage'][0]['#markup']; ?></a></span>
                          </div>
                        </div>

                        <div class="personal_fp_hours">
                          <h2>&nbsp;</h2>
                        </div>

                      <?php endif; ?>
                    <?php else: ?>

                      <?php if (isset($page['uw-virtual-site-homepage']['field_uw_office_location'])
                        && isset($page['uw-virtual-site-homepage']['field_uw_phone_number'][0]['#markup'])): ?>

                        <div class="personal_fp_contact">
                          <h2>Contact</h2>
                          <div><span><?php print render($page['uw-virtual-site-homepage']['field_uw_office_location']); ?></span></div>
                        </div>

                        <div class="personal_fp_hours">
                          <h2>&nbsp;</h2>
                          <div>
                            <?php if (isset($page['uw-virtual-site-homepage']['field_uw_extension'][0]['#markup'])): ?>
                              <span><?php print $page['uw-virtual-site-homepage']['field_uw_phone_number'][0]['#markup'] . ', ext. ' . $page['uw-virtual-site-homepage']['field_uw_extension'][0]['#markup']; ?></span>
                            <?php else: ?>
                              <span><?php print $page['uw-virtual-site-homepage']['field_uw_phone_number'][0]['#markup']; ?></span>
                            <?php endif ?>
                          </div>
                        </div>

                      <?php endif; ?>

                      <?php if (isset($page['uw-virtual-site-homepage']['field_uw_office_location'])
                            && !isset($page['uw-virtual-site-homepage']['field_uw_phone_number'][0]['#markup'])): ?>

                        <div class="personal_fp_contact">
                          <h2>Contact</h2>
                          <div><span><?php print render($page['uw-virtual-site-homepage']['field_uw_office_location']); ?></span></div>
                        </div>

                        <div class="personal_fp_hours">
                          <h2>&nbsp;</h2>
                        </div>

                      <?php endif; ?>

                      <?php if (!isset($page['uw-virtual-site-homepage']['field_uw_office_location'])
                             && isset($page['uw-virtual-site-homepage']['field_uw_phone_number'][0]['#markup'])): ?>

                        <div class="personal_fp_contact">
                          <h2>Contact</h2>
                          <div>
                            <?php if(isset($page['uw-virtual-site-homepage']['field_uw_extension'][0]['#markup'])): ?>
                              <span><?php print $page['uw-virtual-site-homepage']['field_uw_phone_number'][0]['#markup'] . ', ext. ' . $page['uw-virtual-site-homepage']['field_uw_extension'][0]['#markup']; ?></span>
                            <?php else: ?>
                              <span><?php print $page['uw-virtual-site-homepage']['field_uw_phone_number'][0]['#markup']; ?>
                            <?php endif ?>
                          </div>
                        </div>

                        <div class="personal_fp_hours">
                          <h2>&nbsp;</h2>
                        </div>

                      <?php endif; ?>

                    <?php endif; ?>

                  <?php else: ?>

                    <div class="personal_fp_contact"></div>
                    <div class="personal_fp_hours"></div>
                  <?php endif; ?>

                <?php endif; ?>

                </div>
              </div>
            </div>
          </div>
          <!-- /site inner -->
        <?php endif; ?>

        <div id="extradiv"></div>

        <div id="branding_footer">
          <div class="branding-container">
            <div class="uw-footer">
              <div class="uw-site-footer--global">
                <div class="uw-section--inner">
                  <div class="uw-site-flex">
                    <div id="block-uw-nav-global-footer-footer-1" class="block block-uw-nav-global-footer contextual-links-region block-odd footer-1  non_generic_footer">
                      <div id="uw-footer-address" about="//uwaterloo.ca/" typeof="v:VCard">

                        <div class="element-hidden">
                          <div property="v:fn">University of Waterloo</div>
                          <div rel="v:org">
                            <div property="v:organisation-name">University of Waterloo</div>
                          </div>
                          <div rel="v:geo">
                            <div property="v:latitude">43.471468</div>
                            <div property="v:longitude">-80.544205</div>
                          </div>
                        </div>

                        <div rel="v:adr">
                          <div property="v:street-address">200 University Avenue West</div>
                        <div>

                        <span property="v:locality">Waterloo</span>,
                        <span property="v:region">ON</span>,
                        <span property="v:country-name">Canada</span>&nbsp;
                        <span property="v:postal-code">N2L 3G1</span>

                      </div>
                    </div>

                    <div class="uw-footer-phone" rel="v:tel">
                      <a href="tel:+1-519-888-4567" property="rdf:value">+1 519 888 4567</a>
                    </div>

                  </div>
                </div>

                <div id="block-uw-nav-global-footer-footer-2" class="block block-uw-nav-global-footer contextual-links-region block-even footer-2">

                  <ul class="global-menu">

                    <li><a href="//uwaterloo.ca/about/how-find-us/contact-waterloo">Contact Waterloo</a></li>
                    <li><a href="//uwaterloo.ca/map/">Maps &amp; Directions</a></li>
                    <li><a href="//uwaterloo.ca/watsafe/">WatSAFE</a></li>
                    <li><a href="//uwaterloo.ca/human-resources/accessibility">Accessibility</a></li>
                    <li><a href="//uwaterloo.ca/privacy/">Privacy</a></li>
                    <li><a href="//uwaterloo.ca/copyright">Copyright</a></li>
                    <li><a href="//uwaterloo.ca/news/">Media</a></li>
                    <li><a href="//uwaterloo.ca/careers/">Careers</a></li>
                    <li><a href="//uwaterloo.ca/about/how-find-us/contact-waterloo/contact-form" accesskey="9">Feedback</a></li>

                  </ul>

                </div>

                <div id="block-uw-nav-global-footer-footer-3" class="block block-uw-nav-global-footer contextual-links-region block-odd footer-3">

                  <div class="uw-footer-social-media">

                    <ul class="uw-footer-social">
                      <li><a href="https://www.facebook.com/university.waterloo"><i class="ifdsu fdsu-facebook"></i><span class="off-screen">facebook</span></a></li>
                      <li><a href="https://twitter.com/uWaterloo"><i class="ifdsu fdsu-twitter"></i><span class="off-screen">twitter</span></a></li>
                      <li><a href="https://www.youtube.com/uwaterloo"><i class="ifdsu fdsu-youtube"></i><span class="off-screen">youtube</span></a></li>
                      <li><a href="https://instagram.com/uofwaterloo/"><i class="ifdsu fdsu-instagram"></i><span class="off-screen">instagram</span></a></li>
                      <li><a href="https://www.linkedin.com/company/university-of-waterloo"><i class="ifdsu fdsu-linkedin"></i><span class="off-screen">linkedin</span></a></li>

                    </ul>

                    <div class="uw-footer-social-directory">
                      <a href="https://uwaterloo.ca/social-media/">@uwaterloo social directory</a>
                    </div>

                  </div>

                  <div id="cas_login">
                    <?php if(user_is_logged_in()): ?>
                      <a href="<?php print variable_get('cas_logout_url'); ?>">Log out</a>
                    <?php else: ?>
                      <a href="<?php print variable_get('cas_login_url'); ?>">Log in</a>
                    <?php endif; ?>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div><!-- /page_wrap -->
