/**
 * @file
 */
module.exports = (function () {
  'use strict';
  var pkg = require('./package.json'),
      gulp = require('gulp'),
      glob = require('glob'),
      gutil = require('gulp-util'),
      minifyCSS = require('gulp-minify-css'),
      pngquant = require('imagemin-pngquant'),
      plugins = require('gulp-load-plugins')(),
      browserSync = require('browser-sync').create();
  var config = {
    sass:'./sass/**/*.{scss,sass}',
    sassSrc:'./sass/rwd.scss',
    sassIe:'./sass/ie.scss',
    css:'./css',
    js:'./scripts',
    jsSrc:'./js/resp.js',
    images:'./images/*.{png,gif,jpeg,jpg,svg}',
    imagesmin:'./images/minified',
  };
  var AUTOPREFIXER_BROWSERS = [
    '> 1%',
    'ie >= 8',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 4',
    'bb >= 10'
  ];
  var onError = function(err) {
    gutil.log(err);
    this.emit('end');
  };
  gulp.task('styles',function() {
    return gulp.src(config.sassSrc)
       // .pipe(plugins.sourcemaps.init())
      .pipe(plugins.plumber())
      .pipe(plugins.sass({
        includePaths: require('node-bourbon').includePaths,
        outputStyle: 'collapsed'
      }))
        .on('error', function (err) {
          gutil.log(err);
          gutil.beep();
          this.emit('end');
        })
      .pipe(plugins.autoprefixer({
        browsers:AUTOPREFIXER_BROWSERS,
        cascade: false
      }))
        .pipe(minifyCSS())
        .pipe(plugins.concat('rwd.css'))
        .pipe(gulp.dest(config.css))
        .pipe(browserSync.stream())
        .pipe(plugins.size({title:'css'}));
  });
  gulp.task('scripts',function() {
    return gulp.src(config.jsSrc)
        .on('error', function (err) {
            gutil.log(err);
            this.emit('end');
            growlNotifier({
              title: 'js',
              message: 'error'
            });
        })
        .pipe(plugins.jshint())
        .pipe(plugins.jshint.reporter('jshint-stylish'))
        // .pipe(plugins.jshint.reporter('fail'));
        .pipe(plugins.uglify())
        // .pipe(plugins.rename({suffix:".min"}))
        .pipe(gulp.dest(config.js));
  });
  gulp.task('drush', plugins.shell.task([
    'drush cache-clear theme-registry'
  ]));
  gulp.task('default',[],function() {
    gulp.start('styles','scripts','watch');
  });
  gulp.task('watch',[],function() {
    gulp.watch(config.jsSrc,['scripts']);
    gulp.watch(config.sass,['styles']);
    gulp.watch(config.images,['images']);
    // gulp.watch('**/*.{php,inc,info}',['drush']);
  });
})();